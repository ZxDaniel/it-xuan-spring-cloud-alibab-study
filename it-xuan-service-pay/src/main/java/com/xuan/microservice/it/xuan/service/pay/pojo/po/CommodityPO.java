package com.xuan.microservice.it.xuan.service.pay.pojo.po;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.pay.pojo.po
 * @date Create Date 2020/11/26 11:27
 * @Description: 商品实体类
 */
@Data
@Entity
@Table(name="t_commodity")
public class CommodityPO {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic
    @Column(name = "commodity_name")
    private String commodityName;

    @Basic
    @Column(name = "commodity_price")
    private BigDecimal commodityPrice;

    @Basic
    @Column(name = "sold_num")
    private Integer soldNum;
}
