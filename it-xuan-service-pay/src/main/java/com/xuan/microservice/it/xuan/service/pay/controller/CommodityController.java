package com.xuan.microservice.it.xuan.service.pay.controller;

import com.plumelog.trace.annotation.Trace;
import com.xuan.microservice.it.xuan.service.pay.pojo.vo.PayReqVO;
import com.xuan.microservice.it.xuan.service.pay.pojo.vo.PayResVO;
import com.xuan.microservice.it.xuan.service.pay.service.CommodityService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.pay.controller
 * @date Create Date 2020/11/26 11:39
 * @Description: 请输入该类的功能描述
 */
@RestController
public class CommodityController {

    @Resource(type = CommodityService.class)
    private CommodityService commodityService;

    @Trace
    @PostMapping("/pay")
    public PayResVO pay(@RequestBody PayReqVO payReqVO){
        return commodityService.pay(payReqVO);
    }
}
