package com.xuan.microservice.it.xuan.service.pay.pojo.vo;

import lombok.Data;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.pay.pojo.vo
 * @date Create Date 2020/11/26 11:43
 * @Description: 请输入该类的功能描述
 */
@Data
public class PayResVO {

    private String message;

}
