package com.xuan.microservice.it.xuan.service.pay.service;

import com.xuan.microservice.it.xuan.service.pay.pojo.vo.PayReqVO;
import com.xuan.microservice.it.xuan.service.pay.pojo.vo.PayResVO;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.pay.service
 * @date Create Date 2020/11/26 12:20
 * @Description: 请输入该类的功能描述
 */
public interface CommodityService {

    PayResVO pay(PayReqVO payReqVO);
}
