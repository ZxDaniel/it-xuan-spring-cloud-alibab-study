package com.xuan.microservice.it.xuan.service.pay.dao;

import com.xuan.microservice.it.xuan.service.pay.pojo.po.CommodityPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.pay.dao
 * @date Create Date 2020/11/26 11:35
 * @Description: 请输入该类的功能描述
 */
@Repository
public interface CommodityDao extends JpaRepository<CommodityPO,Integer> {
}
