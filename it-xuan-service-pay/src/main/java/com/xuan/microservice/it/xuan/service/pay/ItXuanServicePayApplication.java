package com.xuan.microservice.it.xuan.service.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Administrator
 */
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.xuan.microservice.it.xuan.service.pay","com.plumelog"})
public class ItXuanServicePayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItXuanServicePayApplication.class, args);
    }

}
