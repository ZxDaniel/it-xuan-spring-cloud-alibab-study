package com.xuan.microservice.it.xuan.service.pay.pojo.vo;

import lombok.Data;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.pay.pojo
 * @date Create Date 2020/11/26 11:42
 * @Description: 请输入该类的功能描述
 */
@Data
public class PayReqVO {


    private Integer commodityId;


    private Integer soldNum;
}
