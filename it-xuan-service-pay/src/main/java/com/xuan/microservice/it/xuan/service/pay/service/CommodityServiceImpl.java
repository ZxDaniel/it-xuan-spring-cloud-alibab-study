package com.xuan.microservice.it.xuan.service.pay.service;

import com.plumelog.trace.annotation.Trace;
import com.xuan.microservice.dubbo.sample.api.pojo.vo.PayOperatingVO;
import com.xuan.microservice.dubbo.sample.api.service.UserDubboService;
import com.xuan.microservice.dubbo.sample.api.service.WarehouseDubboService;
import com.xuan.microservice.it.xuan.service.pay.dao.CommodityDao;
import com.xuan.microservice.it.xuan.service.pay.pojo.po.CommodityPO;
import com.xuan.microservice.it.xuan.service.pay.pojo.vo.PayReqVO;
import com.xuan.microservice.it.xuan.service.pay.pojo.vo.PayResVO;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.MDC;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.pay.service
 * @date Create Date 2020/11/26 11:39
 * @Description: 请输入该类的功能描述
 */
@Slf4j
@DubboService
public class CommodityServiceImpl implements CommodityService{

    @Resource(type = CommodityDao.class)
    private CommodityDao commodityDao;

    @DubboReference
    private UserDubboService userDubboService;

    @DubboReference
    private WarehouseDubboService warehouseDubboService;

    @Trace
    @Override
    @GlobalTransactional(timeoutMills = 300000, name = "spring-cloud-demo-tx")
    public PayResVO pay(PayReqVO payReqVO) {
        log.info("支付服务开始");
        MDC.put("orderid", "1");
        MDC.put("userid", "4");
        MDC.get("userid");
        log.info("扩展字段");
        Optional<CommodityPO> commodityOptional = commodityDao.findById(payReqVO.getCommodityId());
        if (commodityOptional.isPresent()){
            CommodityPO commodity = commodityOptional.get();
            PayOperatingVO payOperating = new PayOperatingVO();
            payOperating.setCommodityId(payReqVO.getCommodityId());
            payOperating.setUserId(1);
            payOperating.setCommodityNum(payReqVO.getSoldNum());
            payOperating.setCommodityPrice(commodity.getCommodityPrice().multiply(new BigDecimal(payReqVO.getSoldNum())));
            log.info("userDubboService_rpc调用开始");
            userDubboService.deductMoney(payOperating);
            log.info("userDubboService_rpc调用结束");
            log.info("warehouseDubboService_rpc调用开始");
            warehouseDubboService.reduceInventory(payOperating);
            log.info("warehouseDubboService_rpc调用结束");
        }
        PayResVO payRes = new PayResVO();
        payRes.setMessage("是成功了呢");
        log.info("支付结束");
        return payRes;
    }
}
