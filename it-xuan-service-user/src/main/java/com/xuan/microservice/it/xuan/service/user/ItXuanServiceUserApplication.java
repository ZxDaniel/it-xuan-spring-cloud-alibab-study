package com.xuan.microservice.it.xuan.service.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Administrator
 */
@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.xuan.microservice.it.xuan.service.user","com.plumelog"})
public class ItXuanServiceUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItXuanServiceUserApplication.class, args);
    }

}
