package com.xuan.microservice.it.xuan.service.user.pojo.po;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.user.pojo.po
 * @date Create Date 2020/11/26 14:08
 * @Description: 请输入该类的功能描述
 */
@Data
@Entity
@Table(name="t_user")
public class UserPO {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "deposit")
    private BigDecimal deposit;
}
