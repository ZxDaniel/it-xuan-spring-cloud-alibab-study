package com.xuan.microservice.it.xuan.service.user.controller;

import com.xuan.microservice.dubbo.sample.api.service.SayHello;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.user.controller
 * @date Create Date 2020/11/24 16:32
 * @Description: dubbo控制器入口
 */
@RestController
public class SayHelloController {

    @DubboReference
    private SayHello sayHello;

    @GetMapping("/echo")
    public String echo(String message) {
        return sayHello.sayHello(message);
    }
}
