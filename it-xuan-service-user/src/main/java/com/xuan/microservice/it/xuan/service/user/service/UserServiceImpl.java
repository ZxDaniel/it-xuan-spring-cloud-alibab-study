package com.xuan.microservice.it.xuan.service.user.service;

import com.plumelog.trace.annotation.Trace;
import com.xuan.microservice.dubbo.sample.api.pojo.vo.PayOperatingVO;
import com.xuan.microservice.dubbo.sample.api.service.UserDubboService;
import com.xuan.microservice.it.xuan.service.user.dao.UserDao;
import com.xuan.microservice.it.xuan.service.user.pojo.po.UserPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.user.service
 * @date Create Date 2020/11/26 14:10
 * @Description: 请输入该类的功能描述
 */
@Slf4j
@DubboService
public class UserServiceImpl implements UserDubboService {

    @Autowired
    private UserDao userDao;

    @Trace
    @Override
    public void deductMoney(PayOperatingVO payOperatingVO) {
        log.info("用户服务开始调用");
        MDC.put("userId", payOperatingVO.getUserId().toString());
        Optional<UserPO> userOptional = userDao.findById(payOperatingVO.getUserId());
        if (userOptional.isPresent()){
            UserPO user = userOptional.get();
            user.setDeposit(user.getDeposit().subtract(payOperatingVO.getCommodityPrice()));
            userDao.saveAndFlush(user);
        }
        log.info("用户服务结束调用");
    }
}
