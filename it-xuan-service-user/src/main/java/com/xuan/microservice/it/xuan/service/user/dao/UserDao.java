package com.xuan.microservice.it.xuan.service.user.dao;

import com.xuan.microservice.it.xuan.service.user.pojo.po.UserPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.user.dao
 * @date Create Date 2020/11/26 14:10
 * @Description: 请输入该类的功能描述
 */
@Repository
public interface UserDao extends JpaRepository<UserPO, Integer> {
}
