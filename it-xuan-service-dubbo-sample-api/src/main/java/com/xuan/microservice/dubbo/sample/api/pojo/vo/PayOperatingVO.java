package com.xuan.microservice.dubbo.sample.api.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.dubbo.sample.api.pojo.vo
 * @date Create Date 2020/11/26 14:16
 * @Description: 请输入该类的功能描述
 */
@Data
public class PayOperatingVO implements Serializable {

    private Integer commodityId;

    private Integer commodityNum;

    private Integer userId;

    private BigDecimal commodityPrice;
}
