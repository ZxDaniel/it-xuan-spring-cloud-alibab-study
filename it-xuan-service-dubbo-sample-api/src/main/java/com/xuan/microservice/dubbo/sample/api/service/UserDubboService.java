package com.xuan.microservice.dubbo.sample.api.service;

import com.xuan.microservice.dubbo.sample.api.pojo.vo.PayOperatingVO;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.dubbo.sample.api.service
 * @date Create Date 2020/11/26 14:29
 * @Description: 请输入该类的功能描述
 */
public interface UserDubboService {

    /**
     * 进行用户扣款
     * @param payOperating 钱数
     */
    void deductMoney(PayOperatingVO payOperating);
}
