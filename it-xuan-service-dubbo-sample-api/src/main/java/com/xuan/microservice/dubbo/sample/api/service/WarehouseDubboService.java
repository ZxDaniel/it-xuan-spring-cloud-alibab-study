package com.xuan.microservice.dubbo.sample.api.service;

import com.xuan.microservice.dubbo.sample.api.pojo.vo.PayOperatingVO;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.dubbo.sample.api.service
 * @date Create Date 2020/11/26 14:30
 * @Description: 请输入该类的功能描述
 */
public interface WarehouseDubboService {

    /**
     * 进行库存扣除
     * @param payOperating 商品数
     */
    void reduceInventory(PayOperatingVO payOperating);
}
