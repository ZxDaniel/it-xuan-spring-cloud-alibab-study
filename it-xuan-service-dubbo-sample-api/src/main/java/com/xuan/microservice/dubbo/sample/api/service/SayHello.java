package com.xuan.microservice.dubbo.sample.api.service;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.dubbo.sample.api
 * @date Create Date 2020/11/24 16:09
 * @Description: dubbo测试
 */
public interface SayHello {

    /**
     * dubbo的测试方法
     * @param message 传入的信息
     * @return 输入的消息
     */
    String sayHello(String message);
}
