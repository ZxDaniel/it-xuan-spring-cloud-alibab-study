package com.xuan.microservice.itxuancommongateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Administrator
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ItXuanCommonGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItXuanCommonGatewayApplication.class, args);
    }

}
