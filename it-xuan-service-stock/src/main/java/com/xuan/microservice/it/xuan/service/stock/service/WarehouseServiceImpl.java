package com.xuan.microservice.it.xuan.service.stock.service;

import com.plumelog.trace.annotation.Trace;
import com.xuan.microservice.dubbo.sample.api.pojo.vo.PayOperatingVO;
import com.xuan.microservice.dubbo.sample.api.service.WarehouseDubboService;
import com.xuan.microservice.it.xuan.service.stock.dao.WarehouseDao;
import com.xuan.microservice.it.xuan.service.stock.pojo.po.WarehousePO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.data.domain.Example;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.stock.service
 * @date Create Date 2020/11/26 14:05
 * @Description: 请输入该类的功能描述
 */
@Slf4j
@DubboService
public class WarehouseServiceImpl implements WarehouseDubboService {

    @Resource(type = WarehouseDao.class)
    private WarehouseDao warehouseDao;

    /**
     * 扣库存
     * @param payOperatingVO 扣库存
     */
    @Trace
    @Override
    public void reduceInventory(PayOperatingVO payOperatingVO) {
        log.info("仓库服务开始调用");
        WarehousePO warehouse = new WarehousePO();
        warehouse.setCommodityId(payOperatingVO.getCommodityId());
        Optional<WarehousePO> warehouseOptional = warehouseDao.findOne(Example.of(warehouse));
        if (warehouseOptional.isPresent()){
            WarehousePO warehouseDb = warehouseOptional.get();
            warehouseDb.setCommodityNum(warehouseDb.getCommodityNum() - payOperatingVO.getCommodityNum());
            warehouseDao.saveAndFlush(warehouseDb);
        }
        log.info("仓库服务结束调用");
    }
}
