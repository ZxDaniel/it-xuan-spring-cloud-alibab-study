package com.xuan.microservice.it.xuan.service.stock.dao;

import com.xuan.microservice.it.xuan.service.stock.pojo.po.WarehousePO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.stock.dao
 * @date Create Date 2020/11/26 14:02
 * @Description: 请输入该类的功能描述
 */
@Repository
public interface WarehouseDao extends JpaRepository<WarehousePO, Integer> {

}
