package com.xuan.microservice.it.xuan.service.stock.pojo.po;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.stock.pojo.po
 * @date Create Date 2020/11/26 14:01
 * @Description: 库存映射
 */
@Data
@Entity
@Table(name="t_warehouse")
public class WarehousePO {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Basic
    @Column(name = "commodity_id")
    private Integer commodityId;

    @Basic
    @Column(name = "commodity_num")
    private Integer commodityNum;

}
