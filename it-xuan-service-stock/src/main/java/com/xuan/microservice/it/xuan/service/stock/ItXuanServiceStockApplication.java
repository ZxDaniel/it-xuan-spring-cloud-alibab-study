package com.xuan.microservice.it.xuan.service.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Administrator
 */
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.xuan.microservice.it.xuan.service.stock","com.plumelog"})
public class ItXuanServiceStockApplication {

//    static {
//        //进行日志增强，自动判断日志框架
//        AspectLogEnhance.enhance();
//    }


    public static void main(String[] args) {
        SpringApplication.run(ItXuanServiceStockApplication.class, args);
    }

}
