package com.xuan.microservice.it.xuan.service.stock.service;

import com.xuan.microservice.dubbo.sample.api.service.SayHello;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author zhangxuan@e6yun.com
 * @version V1.0
 * @Package com.xuan.microservice.it.xuan.service.stock.service
 * @date Create Date 2020/11/24 16:14
 * @Description: Dubbo 测试实现类
 */
@DubboService
public class SayHelloImpl implements SayHello {


    @Override
    public String sayHello(String s) {
        System.out.println("得到传输的参数" + s);
        return "恭喜你完成的dubbo的调用";
    }
}
